import React,{Component} from 'react';
import axios from 'axios';
import {AppContextProvider} from '../../store';


export default class AppProvider extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      appLoading: true,
      currentItem: null,
      currentCostItems: [],
      selectedPizza:[],
      selectedToppings: [],
      totalCost: 0.00,
      pizzaList: []
    };
    this.selectPizzaSize = this.selectPizzaSize.bind(this);
    this.togglePizzaTopping = this.togglePizzaTopping.bind(this);
    this.selectTopping = this.selectTopping.bind(this);
    this.removePizzaFromCart = this.removePizzaFromCart.bind(this);
  }

  async componentDidMount() {
       const url = ' https://core-graphql.dev.waldo.photos/pizza';
       const payload = await this.getPizzaData(url);
       this.setState({appLoading: false, pizzaList:payload})
       console.log('my payload', payload);

  }

   getPizzaData(url) {
     const query =`query{
       pizzaSizes {
         name,
         basePrice,
         maxToppings
         toppings{
           topping{
             price
             name
           }
           defaultSelected
         }
       }
     }`;

     return axios({
       url,
       method:'post',
       data:{query}
     }).then((value) => value.data.data).catch((err) => err)
   }


  selectPizzaSize(item) {
    const list = this.state.selectedPizza;
    let dup = [];
    if(list.length > 0) {
       dup = list.filter((pizza) => pizza.name == item.name);
       if(dup.length > 0) {
         return;
       } else {
           this.updateSelectedItem(item);
       }
    } else {
       this.updateSelectedItem(item);
    }
  }

 updateSelectedItem(item) {
   const selectedPizza = this.state.selectedPizza.concat([item]);
   this.setState({selectedPizza}, () => {
       this.getPurchaseTotal(item.basePrice);
   });
 }

  selectTopping(selectedPizza,  topping) {
    const newList =  this.updateToppingList(selectedPizza, topping);
    const updatedTopping = Object.assign({}, this.state, newList);
    this.setState(updatedTopping);
  }

  updateToppingList(selectedPizza, topping) {
    const toppingList = selectedPizza.toppings;
    for (let i = 0; i < toppingList.length; i++) {
        if(topping == toppingList[i].topping.name){
          toppingList[i].defaultSelected = !toppingList[i].defaultSelected;
        }
    }
    mergeSelectedToppingPrice(toppingList, topping)
    return selectedPizza;
  }

  removePizzaFromCart(pizzaItem) {
     const updatedList = this.state.selectedPizza.filter(item => pizzaItem.name !== item.name);
     this.setState({selectedPizza:updatedList}, ()=> {

       this.updatePrice(pizzaItem.basePrice);
     });
  }

  togglePizzaTopping(e, topping){
    if(e.target.checked){
      const selectedToppings = this.state.selectedToppings.concat([topping])
      console.log('new topping', topping, selectedToppings);
      this.setState({selectedToppings});
    }

  }

  updatePrice(selectedItem) {
    const items = this.state.currentCostItems.reduce((a, b) => a + b, 0);
     const updatedList = this.state.currentCostItems.filter(item => selectedItem !== item);
     this.setState({currentCostItems: updatedList}, () => {
         const currentCostItems = this.state.currentCostItems.reduce((a, b) => a + b, 0);
         this.setState({totalCost: currentCostItems.toFixed(2)});
     })
  }

  getPurchaseTotal(price) {
    const currentCost = this.state.currentCostItems.concat([price]);
    this.setState({currentCostItems: currentCost}, () => {
        const currentCostItems = this.state.currentCostItems.reduce((a, b) => a + b, 0);
        this.setState({totalCost: currentCostItems.toFixed(2)});
    })
  }

  mergeSelectedToppingPrice(toppings, topping) {

    for (let i = 0; i < toppings.length; i++) {
      if((toppings[i].topping.name == topping) && toppings[i].defaultSelected) {
        this.state.currentCostItems.concat([toppings[i].topping.price])
      }
    }
    this.setState(this.state.currentCostItems);
    //return this.state.currentCostItems;
  }

  render(){
    const {
      selectPizzaSize,
      togglePizzaTopping,
      removePizzaFromCart,
      selectTopping
    } = this;
    return(
      <AppContextProvider value={
          {...this.state,
            selectPizzaSize,
            selectTopping,
            togglePizzaTopping,
            removePizzaFromCart
          }
        }>{this.props.children}</AppContextProvider>
    );
  }

}
