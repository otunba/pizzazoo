import React, {Component, Fragment} from 'react';
import CartItem from './cartItem'


class ShoppingCart extends Component{
  constructor(props){
  	super(props);
  	this.state = {};
  }

  render(){
    const {pizzaList, removeItem, baseCost, totalCost} = this.props;
    return(
      <Fragment>
        <ul>
          {pizzaList.map((pizza, i) =>
            <CartItem pizza={pizza} key={i} removePizza={removeItem}/>)
          }
      </ul>
      <div className="totalcost-block">
        <p>Total Cost:</p>
        <i className="fas fa-dollar-sign"/>
        <div className="cartItemPrice">{totalCost}</div>
        </div>
      </Fragment>
    );
  }

}

 export default ShoppingCart;
