import React from 'react';

const CartItem = (props) => {
  const {name} = props.pizza;
  return (
    <li className="cartItem">
      <i className="far fa-trash-alt" onClick={() => props.removePizza(props.pizza)}/>
      <div className="cartItem">{name}</div>
      <i className="fas fa-dollar-sign"/>
      <div className="cartItemPrice">{props.cartItemCost}</div>
    </li>
  )
}


export default CartItem;
