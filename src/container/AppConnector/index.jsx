import React, {Component} from 'react';
import {AppContextConsumer} from '../../store';

const AppConnector = (AppComponent) => class extends Component {
  render(){
    return(
      <AppContextConsumer>
        {store => <AppComponent {...this.props} {...store}/>}
      </AppContextConsumer>
    );
  }
}

export default AppConnector;
