import React,{PureComponent} from 'react';
import AppConnector from '../AppConnector';
import PizzaList from '../../component/pizzaList';
import ShoppingCart from '../ShoppingCart'
import PizzaToppings from '../../component/toppingList/pizzaToppings';
class PizzaShop extends PureComponent {
  constructor(props){
  	super(props);
  	this.state = {
      currentTopping: []
    };
  }


  render() {
    if(this.props.appLoading) {
      return null;
    }
    const {
      pizzaList,
      selectPizzaSize,
      selectedPizza,
      totalCost,
      selectTopping,
      removePizzaFromCart
    } = this.props;
        return (
            <div className="class-name">
             <PizzaList
                pizzaList={pizzaList}
                selectedPizza={selectedPizza}
                selectTopping={selectTopping}
                selectPizza={selectPizzaSize}
              />
            <h3> Shopping Cart {selectedPizza.length}</h3>
             <ShoppingCart
               pizzaList={selectedPizza}
               removeItem={removePizzaFromCart}
               totalCost={totalCost}
               />

            </div>
        );
  }
}
export default AppConnector(PizzaShop);
