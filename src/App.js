import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PizzaShop from '../src/container/pizzaShop'
import AppProvider from './container/AppProvider'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <AppProvider>
            <PizzaShop/>
          </AppProvider>
        </header>
      </div>
    );
  }
}

export default App;
