import React,{Fragment} from 'react';
import PizzaToppings from '../toppingList/pizzaToppings';
const PizzaItem = (props) => {
  const {name, basePrice, toppings} = props.pizza;
  return (
    <Fragment>
    <li className="pizzaItem" onClick={() => (props.selectPizza(props.pizza))}>
      <div>{name} pizza</div>
      {props.showPrice && <div> [$ {basePrice}]</div>}
    </li>
    <PizzaToppings
      selectedPizza={props.pizza}
      toppings={toppings}
      selectTopping={props.selectTopping}/>
  </Fragment>
  )
}


export default PizzaItem;
