import React from 'react';
import PizzaItem from './pizzaItem';

const PizzaList = ({pizzaList, selectPizza, selectTopping, selectedPizza}) => {
  return(<ul className="pizzaList">
        {pizzaList.pizzaSizes.map((pizza, i) => <PizzaItem pizza={pizza} key={i} selectPizza={selectPizza}
        showPrice
        selectedPizza={selectedPizza}
        selectTopping={selectTopping}/> )}
    </ul>
  );
};


export default PizzaList;
