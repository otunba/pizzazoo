import React, {Fragment} from 'react';
import Topping from './topping';

const PizzaToppings = (props) => {
  console.log('props', props)
  const {toppings, selectedPizza, selectTopping} = props;
  return (
    <Fragment>
    <small> Toppings: </small>
    <ul className="pizzaList">
      {toppings.map((topping, i) =>
        <Topping
          key={i} 
          topping={topping}
          addTopping={selectTopping}
          selectedPizza={selectedPizza}/>)
      }
    </ul>
    </Fragment>
  )
};


export default PizzaToppings;
