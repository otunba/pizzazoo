import React from 'react';


// <input type="checkbox" defaultSelected={false} onChange={() => (props.selectTopping(props.pizza))}>
//  {name}
// </input>

const ToppingItem = (props) => {
  const {name, price} = props.topping.topping;
  return (
    <li className={`${props.topping.defaultSelected ? 'selectedTopping' : ''} pizzaItem`} onClick={()=> props.addTopping(props.selectedPizza,name)}>
      <small>{name}</small>
      <small>${price}</small>
  </li>
 )
}


export default ToppingItem;
